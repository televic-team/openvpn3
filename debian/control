Source: openvpn3
Section: net
Priority: optional
Maintainer: Marc Leeman <marc.leeman@gmail.com>
Build-Depends: debhelper-compat (= 13),
                swig,
                libasio-dev,
                libmbedtls-dev,
                libssl-dev,
                liblz4-dev,
                cmake,
                python3,
                python3-dev,
                pkg-config,
Standards-Version: 4.6.0.1
Rules-Requires-Root: no
Homepage: https://openvpn.net/
Vcs-Git: https://salsa.debian.org/televic-team/openvpn3.git
Vcs-Browser: https://salsa.debian.org/televic-team/openvpn3

Package: libopenvpn3
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: virtual private network core libraries (version 3)
 OpenVPN is an application to securely tunnel IP networks over a
 single UDP or TCP port. It can be used to access remote sites, make
 secure point-to-point connections, enhance wireless security, etc.
 .
 OpenVPN uses all of the encryption, authentication, and certification
 features provided by the OpenSSL library (any cipher, key size, or
 HMAC digest).
 .
 OpenVPN may use static, pre-shared keys or TLS-based dynamic key exchange. It
 also supports VPNs with dynamic endpoints (DHCP or dial-up clients), tunnels
 over NAT or connection-oriented stateful firewalls (such as Linux's iptables).
 .
 This package contains the shared runtime library.

Package: libopenvpn-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libopenvpn3 (= ${binary:Version})
Description: virtual private network (version 3) development headers
 OpenVPN is an application to securely tunnel IP networks over a
 single UDP or TCP port. It can be used to access remote sites, make
 secure point-to-point connections, enhance wireless security, etc.
 .
 OpenVPN uses all of the encryption, authentication, and certification
 features provided by the OpenSSL library (any cipher, key size, or
 HMAC digest).
 .
 OpenVPN may use static, pre-shared keys or TLS-based dynamic key exchange. It
 also supports VPNs with dynamic endpoints (DHCP or dial-up clients), tunnels
 over NAT or connection-oriented stateful firewalls (such as Linux's iptables).
 .
 This package contains the static library and headers for development.
